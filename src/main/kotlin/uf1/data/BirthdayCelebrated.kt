package uf1.data

import java.time.LocalDateTime
import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val dayBirthday = scanner.nextInt()
    val monthBirthday = scanner.nextInt()

    val currentDay = LocalDateTime.now().dayOfMonth
    val currentMonth = LocalDateTime.now().monthValue

    val isCelebrated:Boolean = monthBirthday < currentMonth || monthBirthday == currentMonth && dayBirthday < currentDay

    println(isCelebrated)
}