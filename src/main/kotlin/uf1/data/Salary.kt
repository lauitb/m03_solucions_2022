package uf1.data

import java.util.*

fun main() {
    /**
     *  40 hores de treball la tarifa per hora s'incrementa en un 50%.
     *    gfhgfhj tarifa per hora és de 40 eur/h.
     */

    val scanner = Scanner(System.`in`)
    val horesTreballades = scanner.nextInt()

    val preuPerHora = 40
    val horesMinimes = 40

    val percentatgeHoraExtra = 60 // 50%

    val preuHoraExtra = preuPerHora * percentatgeHoraExtra / 100
    val resultat = horesTreballades * preuPerHora + horesTreballades - horesMinimes * preuPerHora

    println(resultat)

}