package uf1.data

import java.util.*
import kotlin.math.pow

fun main() {
    val scanner = Scanner(System.`in`)
    val diametrePizza = scanner.nextDouble()


    val result = (diametrePizza / 2).pow(2) * Math.PI
    println(result)
}