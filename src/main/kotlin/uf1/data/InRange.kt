package uf1.data

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val userInputValue1 = scanner.nextInt()
    val userInputValue2 = scanner.nextInt()
    val userInputValue3 = scanner.nextInt()
    val userInputValue4 = scanner.nextInt()
    val userInputValue5 = scanner.nextInt()

    val firstRang = userInputValue1..userInputValue2
    val secondRang = userInputValue3..userInputValue4

    val isInRange = userInputValue5 in firstRang && userInputValue5 in secondRang
    println(isInRange)

}