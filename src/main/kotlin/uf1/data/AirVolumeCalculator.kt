package uf1.data

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.US)
    val base = scanner.nextDouble()
    val altura = scanner.nextDouble()
    val longitud = scanner.nextDouble()

    val volum = base*altura*longitud
    println(volum)
}