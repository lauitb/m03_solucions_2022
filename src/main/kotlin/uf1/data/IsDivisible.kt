package uf1.data

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val firstValue = scanner.nextInt()
    val secondValue = scanner.nextInt()

    val isDivisible : Boolean = firstValue % secondValue == 0
}