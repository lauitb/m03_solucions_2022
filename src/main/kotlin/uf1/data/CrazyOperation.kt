package uf1.data

import java.util.*

fun main() {
    /*
    L'usuari escriu 4 enters i s'imprimeix el valor de suma el primer amb el segon,
    mutiplicat per la resta del tercer amb quart.
     */

    val scanner = Scanner(System.`in`)
    val userInputValue1 = scanner.nextInt()
    val userInputValue2 = scanner.nextInt()
    val userInputValue3 = scanner.nextInt()
    val userInputValue4 = scanner.nextInt()

    val sumaPrimerSegon= userInputValue1 + userInputValue2
    val restaTercerQuart = userInputValue3 - userInputValue4
    val crazyOperation = sumaPrimerSegon * restaTercerQuart
    println(crazyOperation)
}