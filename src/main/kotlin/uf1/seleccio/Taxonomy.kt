package uf1.seleccio

import java.util.*

/*
1.
a) Tenen cèl·lules procariotes . .  . . . . REGNE DE LES MONERES
b) Tenen cèl·lules eucariotes . . . . . . . . . . . . . . . . Anar a 2
2.
a) No tenen teixits . . . . . . . . . . . . . . . . . . . .Anar a 3
b) Sí tenen teixits . . . . . . . . . . . . . . . . . . . . . Anar a 4
3.
a) Nutrició autòtrofa o heteròtrofa . . . .  REGNE DELS PROTOCTISTS
b) Nutrició heteròtrofa .. . . . . . . . . . REGNE DELS FONGS
4.
a) Nutrició autòtrofa . .  . . . . . . . . . . . . REGNE DE LES PLANTES
b) Nutrició heteròtrofa . . . . .. . . . . . . . . . REGNE DELS ANIMALS
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val especie = scanner.nextLine()

    println("Tenen cèl·lules procariotes?")
    val esProcariotaString = scanner.nextLine()

    var esProcariota:Boolean = esProcariotaString=="si"


    println("Té teixits?")
    val teTeixitsString = scanner.nextLine()

    var teTeixits = teTeixitsString=="si"

    println("Fa la nutrició només heteròtrofa?")
    val nomesHeterotrofaString = scanner.nextLine()
    var nomesHeterotrofa = teTeixitsString=="si"

    if (esProcariota) {
        println("és del REGNE DELS FONGS")
    }
    else {
        if (teTeixits){
            if (nomesHeterotrofa){
                println("és del REGNE DELS FONGS")
            }
            else {
                println("és del REGNE DELS PROTOCTISTS")
            }
        }
        else{
            if (nomesHeterotrofa){
                println("és del REGNE DELS ANIMALS")
            }
            else {
                println("és del REGNE DE LES PLANTES")
            }

        }

    }
}