package uf1.iterative

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val userInputValue = scanner.nextInt()

    for (i in 1..9){
        println("$i"+" * "+ userInputValue+ " = "+ i*userInputValue)
    }
}