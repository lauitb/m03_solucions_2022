package uf1.iterative

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val numWowels = scanner.nextInt()

    repeat(numWowels){
        var letter = scanner.next()
        when (letter){
            "a","e","i","o","u" -> print(letter)
        }
    }

}