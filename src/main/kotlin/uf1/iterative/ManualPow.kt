package uf1.iterative

import java.util.*

fun main() {
    val sc = Scanner(System.`in`)
    val baseNumber = sc.nextInt()
    val expNumber = sc.nextInt()

    var result = 1
    var i = 0
    while(i<expNumber){
        result *= baseNumber
        i++
    }
    println(result)
}