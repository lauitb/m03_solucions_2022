package uf1.iterative

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val num = scanner.nextInt()

    var i=0
    while (i<num){
        var j=0
        while (j<i+1){
            print("*")
            j++
        }
        println()
        i++
    }
}