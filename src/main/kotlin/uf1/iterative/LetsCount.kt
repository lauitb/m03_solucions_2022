package uf1.iterative

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val userInputValue = scanner.nextInt()

    repeat(userInputValue){
        print(it+1)
    }
}