package uf1.iterative

fun main() {
    repeat(10){
        println("hello world")
    }

    for (i in 1..10){
        println("hello world")
    }

    var i = 0
    while (i<=10){
        println("hello world")
        i++
    }

    i = 0
    do {
        println("hello world")
        i++
    }while (i<=10)
}