package uf1.iterative

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val countTo = scanner.nextInt()
    val jump = scanner.nextInt()

    for (i in 1..countTo step jump){
        print(i)
    }

}