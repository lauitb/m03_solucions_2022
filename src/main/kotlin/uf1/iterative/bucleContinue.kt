package uf1.iterative

fun main() {
    val a = 3
    val b = 4

    var sum = 0
    var i = 0
    while (i < b) {
        sum += a
        i++
    }
    println(sum)
}