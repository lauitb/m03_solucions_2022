package uf1.iterative

import java.util.*



fun main() {
    val scanner = Scanner(System.`in`)

    val x = scanner.nextInt()
    val y = scanner.nextInt()
    drawSquare(x, y)
}

fun drawSquare(x: Int, y :Int) {
    repeat(y){
        drawLine(x)
        println()
    }
}

fun drawLine(x: Int) {
        repeat(x){
            print("*")
        }
}

