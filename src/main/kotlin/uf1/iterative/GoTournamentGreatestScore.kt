package uf1.iterative

import java.util.*

fun main() {

    val scanner = Scanner(System.`in`)
    var name = scanner.nextLine()

    var nameWinner : String = ""
    var puntuationWinner : Int = 0
    while (name != "END"){
        val puntuation = scanner.nextLine().toInt()
        if (puntuation > puntuationWinner) {
            nameWinner = name
            puntuationWinner = puntuation
        }
        name = scanner.nextLine()
    }
    println("$nameWinner: $puntuationWinner")


}