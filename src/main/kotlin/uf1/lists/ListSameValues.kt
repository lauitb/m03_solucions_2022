package uf1.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val values1 = readIntList(scanner)
    val values2 = readIntList(scanner)

    var isEquals = false
    if (values1.size == values2.size){
        isEquals = true
        for (i in values1.indices){
            if (values1[i]!=values2[i]){
                isEquals= false
                break
            }
        }
    }
    println(isEquals)

    // println(values1 == values2)

    //1 2
    //1 ???
}