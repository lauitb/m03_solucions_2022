package uf1.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)

    val isbn = List(10) { scanner.nextInt() }
    var suma = 0
    for (i in 0..isbn.lastIndex - 1) {
        suma += isbn[i] * i + 1
    }
    var control = suma % 11
    println(control == isbn.last())
}