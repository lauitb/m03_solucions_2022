package uf1.lists

import java.util.*

/*
Even values are added at the end of the list odd values are added at the
begginning
 */
fun main() {
    var strangeOrderList = mutableListOf<Int>()

    val scanner = Scanner(System.`in`)
    var number = scanner.nextInt()

    var i = 0

    while (number != -1) {
        if (i % 2 == 1) strangeOrderList.add(number)
        else strangeOrderList.add(0, number)
        i++
        number = scanner.nextInt()
    }
    println(strangeOrderList)
}