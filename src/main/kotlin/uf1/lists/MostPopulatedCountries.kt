package uf1.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    var totalCountries = scanner.nextInt()
    scanner.nextLine()

//    var countries =  mutableListOf<Country>()
//    repeat(totalCountries){
//        val name = scanner.nextLine()
//        val population = scanner.nextInt()
//        val country = Country(name, population)
//        countries.add(country)
//    }
    var countries = List(totalCountries){Country(scanner.nextLine(),scanner.nextLine().toInt())}
    var populationToCompare = scanner.nextInt()

    for (country in countries){
        if (country.population >= populationToCompare){
            println(country.name)
        }
    }
}

class Country(var name: String, var population: Int) {}

