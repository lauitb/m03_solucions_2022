package uf1.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val numberWorkers = scanner.nextInt()

    var workers = List<Worker>(numberWorkers){ Worker(scanner.next(), scanner.nextInt() ) }

//    var totalSalary = 0
//    for (worker in workers){
//        //worker = workers[i]
//        totalSalary += worker.salary
//    }
//    var avg = totalSalary /workers.size




    var avg = workers.map { it.salary  }.average()
    var lessSalary = workers.filter { it.salary < avg }.map { println(it.name) }

//    for (worker in workers){
//        if (worker.salary < avg ) println(worker.name)
//    }

}

class Worker (val name:String, val salary:Int)