package uf1.lists

import java.util.*

fun main() {
    var students = listOf("Magalí", "Magdalena", "Magí","Màlika", "Manel", "Manela", "Mar", "Marc", "Margalida", "Marçal", "Marcel", "Maria", "Maricel", "Marina", "Marta", "Martí", "Martina", "Mausi", "Mei-Xiu", "Miquel", "Ming", "Mohamed")
    var faltesAssistencia = students.toMutableList()
    val scanner = Scanner(System.`in`)
    var numberList = scanner.nextInt()
    while (numberList != -1){
        faltesAssistencia.removeAt(numberList)
        numberList = scanner.nextInt()
    }
    println(faltesAssistencia)

}