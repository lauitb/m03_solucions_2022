package uf1.lists

import java.util.*

/**
 * Reads an int of lists from user input.
 * The user first introduces the number (N).
 * Later it introduces the integers one by one.
 * @return int list of values introduced of size N
 */
fun readIntList(scanner: Scanner):List<Int>{
    var listSize = scanner.nextInt()
    var myList = MutableList(listSize){scanner.nextInt()}
    return myList
}

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.US)
    val values = readIntList(scanner)
    println(values)

}