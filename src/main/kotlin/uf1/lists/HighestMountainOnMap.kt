package uf1.lists

fun main() {
    val map = listOf(
        listOf(1.5, 1.6, 1.8, 1.7, 1.6),
        listOf(1.5, 2.6, 2.8, 2.7, 1.6),
        listOf(1.5, 4.6, 4.4, 4.9, 1.6),
        listOf(2.5, 1.6, 3.8, 7.7, 3.6),
        listOf(1.5, 2.6, 3.8, 2.7, 1.6)
    )


    var max = map[0][0]
    var posicioI = 0
    var posicioJ = 0
    for (i in 0..map.lastIndex) {
        for (j in 0..map[i].lastIndex) {
            if (map[i][j] > max) {
                max = map[i][j]
                posicioI = i
                posicioJ = j
            }
        }
    }
    println("$posicioJ, $posicioI:  $max metres")

}