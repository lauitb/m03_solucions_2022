package uf1.lists

import java.util.*

fun main() {

    val scanner = Scanner(System.`in`)
    val n = scanner.nextInt()

    val numbers = List(n){scanner.nextInt()}
    var isCapICua = true
    var i = 0
    var j = numbers.lastIndex
    while (isCapICua && i<j){
        if (numbers[i]!=numbers[j]) isCapICua = false
        i++
        j--
    }

    if (isCapICua) println("cap i cua")
    else println("no és cap i cua")


}