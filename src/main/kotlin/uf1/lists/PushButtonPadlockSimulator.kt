package uf1.lists

import java.util.*

fun main() {

    var buttons = MutableList(8){false}

    val scanner = Scanner(System.`in`)
    var value = scanner.nextInt()
    while (value != -1){
        buttons[value] = !buttons[value]
        value = scanner.nextInt()
    }
    println(buttons)
    //buttons.forEach { print("$it ") }
}