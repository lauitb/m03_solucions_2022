package uf1.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    var element = scanner.nextInt()
    val orderedList = mutableListOf<Int>()

    while (element != -1 ){
        for (i in 0..orderedList.lastIndex){
            if (element < orderedList[i]) orderedList.add(i,element)
        }
        if (orderedList.size == 0 || element > orderedList[orderedList.lastIndex]) orderedList.add(element)
        element = scanner.nextInt()
    }
    println(orderedList)
}