package uf1.lists

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val n = scanner.nextInt()

    val values = List(n) { scanner.nextInt() }
    var i = 0
    var j = 1
    var isOrdered = true
    while (j < n) {
        if (values[j] < values[i]) {
            isOrdered = false
            break
        }
        i++
        j++
    }
    if (isOrdered) println("ordenats")
    else println("no ordenats")

}