package uf1.lists

fun main() {
    val matrix = listOf(listOf(2,5,1,6),listOf(23,52,14,36),listOf(23,75,81,64))

    var sum = 0
    for (i in 0..matrix.lastIndex){
        for (j in 0..matrix[i].lastIndex){
            sum+=matrix[i][j]
        }
    }
    matrix.flatten().sumOf { it }
    println(sum)
}